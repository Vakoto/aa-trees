﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace aa_trees
{
    public static class AaTree
    {
        public static AaTree<T> Create<T>(IEnumerable<T> source)
             where T : IComparable
        {
            var tree = new AaTree<T>();
            tree.AddRange(source);
            return tree;
        }

        public static AaTree<T> Create<T>(params T[] source)
            where T : IComparable
        {
            return Create(source.AsEnumerable());
        }
    }

    public class AaTree<T> : IEnumerable<T>
        where T : IComparable
    {
        private AaNode<T> root = null;

        public void Add(T value)
        {
            root = root.Insert(value);
        }

        public void AddRange(IEnumerable<T> values)
        {
            foreach (var value in values)
            {
                Add(value);
            }
        }

        public void Remove(T value)
        {
            root = root.Remove(value);
        }

        public void RemoveRange(IEnumerable<T> values)
        {
            foreach (var value in values)
            {
                Remove(value);
            }
        }

        public bool Contains(T key)
        {
            if (root == null)
            {
                return false;
            }

            var currentNode = root;
            while (currentNode != null)
            {
                var comparisonResult = key.CompareTo(currentNode.Value);
                if (comparisonResult == 0)
                {
                    return true;
                }
                currentNode = comparisonResult < 0 ? currentNode.Left : currentNode.Right;
            }

            return false;
        }

        private IEnumerable<AaNode<T>> GetNodes()
        {
            var stack = new Stack<AaNode<T>>();
            var currentNode = root;

            while (currentNode != null)
            {
                stack.Push(currentNode);
                currentNode = currentNode.Left;
            }

            while (stack.Count > 0)
            {
                var popped = stack.Pop();
                currentNode = popped.Right;

                while (currentNode != null)
                {
                    stack.Push(currentNode);
                    currentNode = currentNode.Left;
                }

                yield return popped;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var node in GetNodes())
            {
                yield return node.Value;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public string ToDot()
        {
            var levels = new Dictionary<int, List<T>>();
            var sb = new StringBuilder();

            sb.Append("digraph G {\n");

            foreach (var node in GetNodes())
            {
                if (node.Left != null)
                {
                    sb.AppendLine($"{node.Value} -> {node.Left.Value};");
                }

                if (node.Right != null)
                {
                    sb.AppendLine($"{node.Value} -> {node.Right.Value};");
                }

                if (!levels.ContainsKey(node.Level))
                {
                    levels[node.Level] = new List<T>();
                }

                levels[node.Level].Add(node.Value);
            }

            foreach (var level in levels)
            {
                sb.Append("{");
                sb.Append($"rank = same; {string.Join("; ", level.Value)}");
                sb.Append("}\n");
            }

            sb.Append("}");
            return sb.ToString();
        }

        private void ToString(AaNode<T> node, StringBuilder sb, int rootLevel)
        {
            if (node == null)
            {
                return;
            }

            ToString(node.Right, sb, rootLevel);
            sb.Append(string.Concat(Enumerable.Repeat("___", rootLevel - node.Level)));
            sb.AppendLine(node.Value.ToString());
            ToString(node.Left, sb, rootLevel);
        }

        public override string ToString()
        {
            if (root == null)
            {
                return string.Empty;
            }

            var sb = new StringBuilder();
            ToString(root, sb, root.Level);
            return sb.ToString();
        }
    }
}
