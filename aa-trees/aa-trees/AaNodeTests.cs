﻿using System;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace aa_trees
{
    [TestFixture]
    public class AaNodeTests
    {
        [Test]
        public void SkewTest()
        {
            var a = AaNode.New('a');
            var b = AaNode.New('b');
            var l = AaNode.New('l', a, b, 2);
            var r = AaNode.New('r');
            var t = AaNode.New('t', l, r, 2);

            var skewed = t.Skew();

            Assert.AreEqual(skewed.Value, 'l');
            Assert.AreEqual(skewed.Left.Value, 'a');
            Assert.AreEqual(skewed.Right.Value, 't');
            Assert.AreEqual(skewed.Right.Left.Value, 'b');
            Assert.AreEqual(skewed.Right.Right.Value, 'r');

            Assert.AreEqual(a.Level, 1);
            Assert.AreEqual(b.Level, 1);
            Assert.AreEqual(r.Level, 1);

            Assert.AreEqual(l.Level, 2);
            Assert.AreEqual(t.Level, 2);
        }

        [Test]
        public void SplitTest()
        {
            var a = AaNode.New('a');
            var b = AaNode.New('b');
            var x = AaNode.New('x', 2);
            var r = AaNode.New('r', b, x, 2);
            var t = AaNode.New('t', a, r, 2);

            var splitted = t.Split();

            Assert.AreEqual(splitted.Value, 'r');
            Assert.AreEqual(splitted.Right.Value, 'x');
            Assert.AreEqual(splitted.Left.Value, 't');
            Assert.AreEqual(splitted.Left.Right.Value, 'b');
            Assert.AreEqual(splitted.Left.Left.Value, 'a');

            Assert.AreEqual(a.Level, 1);
            Assert.AreEqual(b.Level, 1);
            Assert.AreEqual(x.Level, 2);
            Assert.AreEqual(t.Level, 2);
            Assert.AreEqual(r.Level, 3);
        }

        [Test]
        public void InsertTest_ShouldSplit()
        {
            var root = AaNode.New(0).Insert(1).Insert(2);

            Assert.AreEqual(root.Value, 1);
            Assert.AreEqual(root.Left.Value, 0);
            Assert.AreEqual(root.Right.Value, 2);

            Assert.AreEqual(root.Level, 2);
            Assert.AreEqual(root.Left.Level, 1);
            Assert.AreEqual(root.Right.Level, 1);
        }

        [Test]
        public void InsertTest_ShouldSkew()
        {
            var root = AaNode.New(1).Insert(0);

            Assert.AreEqual(root.Value, 0);
            Assert.AreEqual(root.Left, null);
            Assert.AreEqual(root.Right.Value, 1);

            Assert.AreEqual(root.Level, 1);
            Assert.AreEqual(root.Right.Level, 1);
        }

        [Test]
        public void InsertTest_ShouldSkewAndSplit()
        {
            var root = AaNode.New(1).Insert(2).Insert(0);

            Assert.AreEqual(root.Value, 1);
            Assert.AreEqual(root.Left.Value, 0);
            Assert.AreEqual(root.Right.Value, 2);

            Assert.AreEqual(root.Level, 2);
            Assert.AreEqual(root.Left.Level, 1);
            Assert.AreEqual(root.Right.Level, 1);
        }

        [Test]
        public void InsertTest_ValueAlreadyExists()
        {
            var root = AaNode.New(0).Insert(0);
            
            Assert.AreEqual(root.Left, null);
            Assert.AreEqual(root.Right, null);

            Assert.AreEqual(root.Level, 1);
            Assert.AreEqual(root.Value, 0);
        }

        [Test]
        public void RemoveTest_SingleElementTree()
        {
            var root = AaNode.New(0).Remove(0);
            root.Should().BeNull();
        }
    }
}
