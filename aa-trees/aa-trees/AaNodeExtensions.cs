﻿using System;

namespace aa_trees
{
    internal static class AaNodeExtensions
    {
        public static AaNode<T> Skew<T>(this AaNode<T> node)
            where T : IComparable
        {
            if (node == null)
            {
                return null;
            }

            if (node.Left == null)
            {
                return node;
            }

            if (node.Left.Level == node.Level)
            {
                var l = node.Left;
                node.Left = l.Right;
                l.Right = node;

                return l;
            }

            return node;
        }

        public static AaNode<T> Split<T>(this AaNode<T> node)
            where T : IComparable
        {
            if (node == null)
            {
                return null;
            }

            if (node.Right == null || node.Right.Right == null)
            {
                return node;
            }

            if (node.Level == node.Right.Right.Level)
            {
                var r = node.Right;
                node.Right = r.Left;
                r.Left = node;
                r.Level++;

                return r;
            }

            return node;
        }

        public static AaNode<T> Insert<T>(this AaNode<T> node, T value)
            where T : IComparable
        {
            if (node == null)
            {
                return AaNode.New(value);
            }

            var comparisonResult = value.CompareTo(node.Value);
            if (comparisonResult < 0)
            {
                node.Left = Insert(node.Left, value);
            } 
            else if (comparisonResult > 0)
            {
                node.Right = Insert(node.Right, value);
            }

            return node.Skew().Split();
        }

        public static AaNode<T> GetSuccessor<T>(this AaNode<T> node)
            where T : IComparable
        {
            if (node == null || node.Right == null)
            {
                return null;
            }

            var currentNode = node.Right;
            while (currentNode.Left != null)
            {
                currentNode = currentNode.Left;
            }

            return currentNode;
        }

        public static AaNode<T> GetPredecessor<T>(this AaNode<T> node)
            where T : IComparable
        {
            if (node == null || node.Left == null)
            {
                return null;
            }

            var currentNode = node.Left;  
            while (currentNode.Right != null)
            {
                currentNode = currentNode.Right;
            }

            return currentNode;
        }

        public static AaNode<T> UpdateLevel<T>(this AaNode<T> node)
            where T : IComparable
        {
            if (node.IsLeaf)
            {
                return node;
            }

            var shouldBe = 
                (node.Left == null || node.Right == null ? 0 :
                 Math.Min(node.Left.Level, node.Right.Level)) + 1;

            if (shouldBe < node.Level)
            {
                node.Level = shouldBe;

                if (node.Right != null && shouldBe < node.Right.Level)
                {
                    node.Right.Level = shouldBe;
                }
            }

            return node;
        }

        public static AaNode<T> Remove<T>(this AaNode<T> node, T value)
            where T : IComparable
        {
            if (node == null)
            {
                return null;
            }

            var comparisonResult = value.CompareTo(node.Value);

            if (comparisonResult < 0)
            {
                node.Left = Remove(node.Left, value);
            }
            else if (comparisonResult > 0)
            {
                node.Right = Remove(node.Right, value);
            }
            else
            {
                if (node.IsLeaf)
                {
                    return null;
                }
                if (node.Left == null)
                {
                    var successor = GetSuccessor(node);
                    node.Right = Remove(node.Right, successor.Value);
                    node.Value = successor.Value;
                }
                else 
                {
                    var predecessor = GetPredecessor(node);
                    node.Left = Remove(node.Left, predecessor.Value);
                    node.Value = predecessor.Value;
                }
            }

            node = node.UpdateLevel().Skew();
            node.Right = node.Right.Skew();

            if (node.Right != null)
            {
                node.Right.Right = node.Right.Right.Skew();
            }

            node = node.Split();
            node.Right = node.Right.Split();

            return node;
        }
    }
}
