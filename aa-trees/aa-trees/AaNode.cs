﻿using System;

namespace aa_trees
{
    internal static class AaNode
    {
        public static AaNode<T> New<T>(T value, int level = 1)
            where T : IComparable
        {
            return new AaNode<T>(value, level);
        }

        public static AaNode<T> New<T>(T value, AaNode<T> left, AaNode<T> right, int level)
            where T : IComparable
        {
            return new AaNode<T>(value, left, right, level);
        }
    }

    internal class AaNode<T>
        where T : IComparable
    {
        public T Value { get; set; }

        public AaNode<T> Left { get; set; }
 
        public AaNode<T> Right { get; set; }

        public int Level { get; set; }

        public bool IsLeaf => Left == null && Right == null;

        public AaNode(T value, int level = 1)
        {
            Value = value;
            Level = level;
        }

        public AaNode(T value, AaNode<T> left, AaNode<T> right, int level)
        {
            Value = value;
            Left = left;
            Right = right;
            Level = level;
        }
    }
}