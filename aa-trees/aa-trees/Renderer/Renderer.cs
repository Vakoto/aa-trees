﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace aa_trees.Renderer
{
    // Изменённый рендерер из https://github.com/timothy-shields/graphviz
    public class Renderer
    {
        private readonly string graphvizBin;

        public Renderer(string graphvizBin)
        {
            if (graphvizBin == null)
            {
                throw new ArgumentNullException(nameof(graphvizBin));
            }

            this.graphvizBin = graphvizBin;
        }

        public async Task RunAsync(string dot, Stream outputStream, RendererLayouts layout, RendererFormats format, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var fileName = Path.Combine(graphvizBin, GetRendererLayoutExecutable(layout));
            var arguments = GetRendererFormatArgument(format);
            var startInfo = new ProcessStartInfo
            {
                FileName = fileName,
                Arguments = arguments,
                UseShellExecute = false,
                CreateNoWindow = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };
            var tcs = new TaskCompletionSource<object>();
            using (var process = Process.Start(startInfo))
            {
                cancellationToken.ThrowIfCancellationRequested();
                cancellationToken.Register(() =>
                {
                    tcs.TrySetCanceled();
                    process.CloseMainWindow();
                });
                using (var standardInput = process.StandardInput)
                {
                    standardInput.Write(dot);
                }
                using (var standardOutput = process.StandardOutput)
                {
                    await Task.WhenAny(tcs.Task, standardOutput.BaseStream.CopyToAsync(outputStream, 4096, cancellationToken));
                }
            }
            cancellationToken.ThrowIfCancellationRequested();
        }

        private static string Escape(string s)
        {
            const string quote = "\"";
            const string backslash = "\\";
            return quote + s.Replace(quote, backslash + quote) + quote;
        }

        private static string GetRendererLayoutExecutable(RendererLayouts layout)
        {
            switch (layout)
            {
                case RendererLayouts.Dot:
                    return "dot.exe";
                default:
                    throw new ArgumentOutOfRangeException(nameof(layout));
            }
        }

        private static string GetRendererFormatArgument(RendererFormats format)
        {
            switch (format)
            {
                case RendererFormats.Png:
                    return "-Tpng";
                case RendererFormats.Svg:
                    return "-Tsvg";
                default:
                    throw new ArgumentOutOfRangeException(nameof(format));
            }
        }
    }
}
