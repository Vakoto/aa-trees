﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using aa_trees.Renderer;
using FluentAssertions;
using NUnit.Framework;

namespace aa_trees
{
    [TestFixture]
    public class AaTreeTests
    {
        [Test]
        public void GetEnumeratorTest()
        {
            var integers = new[] {1, 2, 3, 4, 5};
            var tree = AaTree.Create(integers);
            tree.Should().BeEquivalentTo(integers);
        }

        [Test]
        public void GetEnumeratorTest_EmptyTree()
        {
            var tree = AaTree.Create(new int[0]);
            tree.Should().BeEmpty();
        }

        [Test]
        public void GetEnumeratorTest_YieldOrder()
        {
            var tree = AaTree.Create(new int[] {1, 2, 0, -2, 3, 10});
            tree.Should().BeInAscendingOrder();
        }

        [Test]
        public void AddTest()
        {
            var tree = new AaTree<int>();
            tree.Add(1);
            tree.Contains(1).Should().BeTrue();
            tree.Contains(0).Should().BeFalse();
        }

        [Test]
        public void ContainsTest()
        {
            var tree = AaTree.Create(new[] {1, 2, 3});
            tree.Should().Contain(new[] {1, 2, 3});
            tree.Should().NotContain(new[] {4, 5, 6});
        }

        [Test]
        public void RemoveTest()
        {
            var source = new[] { 1, 2, 3, 5, 6, -1, -2, -3, -4, -5, -6, 7 };
            var tree = AaTree.Create(source);

            foreach (var item in source)
            {
                tree.Remove(item);
                tree.Should().NotContain(item);
            }
        }
    }
}
