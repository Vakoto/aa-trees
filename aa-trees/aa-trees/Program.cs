﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.NetworkInformation;
using System.Threading;
using aa_trees.Renderer;

namespace aa_trees
{
    class Program
    {
        const string GraphVizBin = @"Graphviz\bin";

        static void PrintTree<T>(AaTree<T> tree, string fileName)
            where T : IComparable
        {
            var renderer = new Renderer.Renderer(GraphVizBin);
            using (Stream file = File.Create(fileName))
            {
                renderer.RunAsync(
                    tree.ToDot(), file,
                    RendererLayouts.Dot,
                    RendererFormats.Png,
                    CancellationToken.None).Wait();
            }
        }

        static void ActionTest<T>(Action<AaTree<T>, T> action, T[] source, T[] actionArgs, string fileName)
            where T : IComparable
        {
            var tree = AaTree.Create(source);
            var i = 0;

            PrintTree(tree, $"{fileName}{i++}.png");

            foreach (var item in actionArgs)
            {
                action(tree, item);
                PrintTree(tree, $"{fileName}{i++}.png");
            }
        }

        static int Menu()
        {
            while (true)
            { 
                Console.WriteLine("1:\tДобавить.");
                Console.WriteLine("2:\tУдалить.");
                Console.WriteLine("3:\tПоискать.");
                Console.WriteLine("4:\tОтпечатать.");
                Console.WriteLine("5:\tВыход");
                Console.Write("Ввод: ");
                var line = Console.ReadLine();
                int result;
                if (int.TryParse(line, out result))
                {
                    return result;
                }
                else
                {
                    Console.WriteLine("Ошибка ввода.");
                    Console.WriteLine();
                }
            } 
        }

        static void Main(string[] args)
        {
            //ActionTest((tree, item) => tree.Remove(item),
            //    new[] {1, 2, 3, 5, 6, -1, -2, -3, -4, -5, -6, 7},
            //    new[] {1, 7, 5},
            //    "treeRemove"
            //);

            //ActionTest((tree, item) => tree.Add(item),
            //    new[] {0},
            //    new[] {10, 13, 15, 30},
            //    "treeAdd"
            //);

            var integerTree = new AaTree<int>();
            var needRepeat = true;
            while (needRepeat)
            {
                int x;
                switch (Menu())
                {
                    case 1:
                        Console.Write("X = ");
                        x = int.Parse(Console.ReadLine());
                        integerTree.Add(x);
                        break;
                    case 2:
                        Console.Write("X = ");
                        x = int.Parse(Console.ReadLine());
                        integerTree.Remove(x);
                        break;
                    case 3:
                        Console.Write("X = ");
                        x = int.Parse(Console.ReadLine());
                        Console.WriteLine($"Contains(x) = {integerTree.Contains(x)}");
                        break;
                    case 4:
                        Console.WriteLine();
                        Console.WriteLine(integerTree.ToString());
                        break;
                    case 5:
                        needRepeat = false;
                        break;
                    default:
                        Console.WriteLine("Неверная опция.");
                        break;
                }
                PrintTree(integerTree, "integerTree.png");
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey();
                Console.Clear();
            }
        }
    }
}
